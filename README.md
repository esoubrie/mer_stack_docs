Welcome to the MER stack documentation
======================================

## [Development](development.md)
Instructions for developing in the Euclid framework and more specifically OU-MER

## [Installation](installation.md)
A guide for people willing to install and run the MER stack and/or the MER pipeline

## [License](LICENSE)

The Euclid license

---

**Happy reading !**  :wink:
