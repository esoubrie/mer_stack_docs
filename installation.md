MER stack and pipeline installation
===================================

The following page will guide you through the installation of the MER pipeline on a LODEEN virtual machine. All the code needed for having this pipeline up and running is located on the [ESA SVN Euclid repository][euclidsvn].

The following table contains the MER modules (official + third-party) required for the pipeline, their custodian and the corresponding SVN host repository

| Project Name          | Custodian        | Project Path                                            |
|---------------------- | ---------------- | ------------------------------------------------------- |
| Background [E]        | [M. Kuemmel][mk] | http://euclid.esac.esa.int/svn/EC/SGS/PF/MER/Background |
| PSFHomogenization [E] | [A. Boucaud][ab] | http://euclid.esac.esa.int/svn/EC/SGS/PF/MER/PSFHomogenization |
|---------------------- | ---------------- | ------------------------------------------------------- |
| T-PHOT                | [E. Merlin][em]  | http://euclid.esac.esa.int/svn/EC/SGS/OU/MER/tphot      |
| READCAT               | [M. Castellano][mc] | http://euclid.esac.esa.int/svn/EC/SGS/OU/MER/READCATv5  |

## Overview

1. [Euclidized modules](#euclidized-modules)
    - [Installation](#installation)
    - [Update](#update)
2. [Third-party modules](#external-modules)
    - [SExtractor](#sextractor)
    - [T-PHOT](#tphot)
    - [READCAT](#readcat)
3. [MER pipeline](#mer-pipeline)
    - [Download](#download)
    - [Testing](#testing)
    - [General use](#general-use)
    - [Troubleshooting](#troubleshooting)

## Euclidized modules

The modules referred to as 'Euclidized' modules ([E]) are modules based on the `Elements` framework. Therefore they follow the official Euclid rules for installation and maintenance.

### Installation

In order to install the official modules on LODEEN, the procedure is the following:

```bash
$ cd ~/Work/Projects
$ svn checkout project_path/trunk project_name

$ cd project_name
$ make all
$ make install
```

It is important that the project name corresponds to the one in the table above, since the pipeline communicates to the different codes through absolute paths. It is much easier to do that than having to rewrite later each occurence of the path in `MERPipe` config files.

> **Note:** Always checkout the content of the trunk/tags/branches into the project name, otherwise LODEEN won't be able to find the `Install Area` directory created with `make install`. That means the developper of a project should maintain one directory for developing purposes with the tags/branches/trunk structure ; and one for actually running the project, installed as explained above.

### Update

Now and then the project modules will be updated. In this case, and especially if you have the trunk version, you should simply run the following command:

```bash
$ cd ~/Work/Projects/project_name
$ make purge
$ svn update

$ make all
$ make install
```


## External modules

Currently the MER pipeline requires [SExtractor](#sextractor), [T-PHOT](#tphot) and [READCAT](#readcat) to be installed.

### SExtractor

There are few possibilities to install SExtractor (v2.19.5) on LODEEN.
Here is one that has worked for me (Alex).

```bash
$ cd ~/Downloads
$ wget http://www.astromatic.net/download/sextractor/sextractor-2.19.5-1.x86_64.rpm

$ rpm -ivh sextractor-2.19.5-1.x86_64.rpm
```

It will download from the [Astromatic website][astromatic] the RPM version of SExtractor (v2.19.5) and install it on LODEEN.


### T-PHOT

The `T-PHOT` installation procedure should be relatively easy on LODEEN since all the dependencies and requirements are already present. The steps are as follows:

```bash
$ cd ~/Work/Projects
$ svn checkout http://euclid.esac.esa.int/svn/EC/SGS/OU/MER/tphot tphot

$ cd tphot
$ sudo ./install_lodeen.py
```

All `T-PHOT` scripts will be compiled and symlinked to the `/usr/bin` directory, so they can be accessed from anywhere within LODEEN.

> **Note:** For more details about the installation process, refer to the `T-PHOT` `README` file
```bash
$ less ~/Work/Projects/tphot/README
```

In case of trouble please check with [Emiliano][em].

### READCAT

The `READCAT` installation on LODEEN is straightforward:

```bash
$ cd ~/Work/Projects
$ svn checkout http://euclid.esac.esa.int/svn/EC/SGS/OU/MER/READCATv5 READCATv5

$ cd READCATv5
$ make
```

> **Note:** In some cases, the path to the `CFITSIO` library might have to be specified in the `Makefile`, e.g. `-L/path/to/cfitsio/lib`.
> **Note2:** The `-lnsl` flag may not work on all machines (it does on LODEEN).

## MER pipeline


### Download

The MER pipeline installation is rather a download procedure.

```bash
$ cd ~/Work/Projects
$ svn checkout http://euclid.esac.esa.int/svn/EC/SGS/OU/MER/MERPipe/trunk MERPipe
```

The `MERPipe` script is purely written in Python and does not need any compilation.
However, one has to care about valid links. If the MER stack installation underwent as described in the previous paragraphs, everything should be fine. Otherwise have a look at `~/Work/Projects/MERPipe/INSTALLATION.txt` for troubleshooting.

And do read the `README.txt` file.

### Testing

Make your first pipeline run with the following command

```bash
$ cd ~/Work/Projects/MERPipe
$ ./MERPipe --all=TEST
```

It should prompt on your terminal the sequential execution of the pipeline.

### General use

Inside the main directory `~/Work/Projects/MERPipe`, there is a directory dedicated to pipeline runs called `MERPipeRuns`, whose structure looks like

```bash
$ tree MERPipeRuns/
MERPipeRuns/
├── DataCache
    └── <dataID>
        ├── g.fits
        ├── g.rms.fits
        ├── ...
        ├── psf_vis.fits
        ├── vis.fits
        └── vis.rms.fits
└── DataSets
    └── <dataID>.data
```

#### Data ID

The `DataSets` folder contains dataset definition files. A given dataset is specified with a data ID `<dataID>` **(capital letters only)** and its content is defined in `<dataID>.data` file. This file contains the input data to run the pipeline, e.g.

```
# 1 IMAGE
# 2 ERROR
# 3 ZP
# 4 BAND
# 5 PSF
# 6 FWHM
# 7 LAMBDA
vis.fits vis.rms.fits 25.943 VIS psf_vis.fits 0.2 805.7
g.fits   g.rms.fits   31.90  G   psf_g.fits   1.0 432.82
r.fits   r.rms.fits   32.32  R   psf_r.fits   0.9 592.11
...
```

where each header line corresponds to a column of the file.
The data files it is referring to, need to be available under the `DataCache/<dataID>` directory, as shown in the above directory tree.

#### Runs

Once the dataset `<dataID>` is set up, the pipeline can be run with the following command:

```bash
$ cd ~/Work/Projects/MERPipe
$ ./MERPipe --all=<dataID>
```

This will create for you a `<runID>` made of the first two letter of the username, the dataset ID and the process number. This will be used to create a new directory in `MERPipeRuns` to store the results of the pipeline run as well as logs on the different processes.

> **Note:** The `<runID>` value can be overwridden with the `--runid=<runID>` option when running the pipeline.

### Troubleshooting

In case of trouble while using the MERPipe, please email [Martin][mk].


---

[![ccbysalogo][imgccbysa]][ccbysa] This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License][ccbysa]

[ccbysa]: http://creativecommons.org/licenses/by-sa/4.0/
[imgccbysa]: https://i.creativecommons.org/l/by-sa/4.0/88x31.png "Creative Commons License"


[mk]: mailto:mkuemmel@usm.lmu.de
[ab]: mailto:alexandre.boucaud@ias.u-psud.fr
[em]: mailto:emiliano.merlin@oa-roma.inaf.it
[sp]: mailto:stefano.pilo@oa-roma.inaf.it
[mc]: mailto:marcello.castellano@oa-roma.inaf.it

[euclidsvn]: http://euclid.esac.esa.int/svn/EC/SGS "Euclid SVN repo"
[astromatic]: http://www.astromatic.net/software "Astromatic software"
